# **Hotki** #

Toohot is a simple Windows tray application. 
It's purpose is to open a (user editable) list of hotkeys for any given, currently active window/application.

# **How to build** #
Run included build.bat. If the build succeeds, the executable wil be located in the root directory of the project.

**IMPORTANT** To compile with the included batch file Visual Studio must be installed. 

# Have fun using, extending or learning Toohot! #

#**Manual**#

**CTRL+ALT+H:** Open hotkeys list for focused window, or create new one.

Right click the tray icon for other options.