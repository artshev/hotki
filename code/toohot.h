#if !defined(TOOHOT_H)

struct list_of_strings
{
    char *String;
    list_of_strings *Next;
};

// TODO(TJOM): make separate struct for LogBuffer
struct prog_values
{
    char *TopWindowClassName;
    char *TopWindowWindowText;
    char *LastOpenWindowText;
    char *CurrentEditorClassName;
    char *FileToExec;
    char *TargetWindowText;
    SENDASYNCPROC Callback;
    list_of_strings *ContentNames;
    list_of_strings *KnownClassNames;
    char *LogBuffer;
};

typedef void hot_key_callback(prog_values *ProgValues);
struct hot_key
{
    UINT Modifiers;
    UINT VKCode;
    hot_key_callback *Callback;
};

static void OpenHotkeysWindow(prog_values *ProgValues);
static void MaximizeLastOpen(prog_values *ProgValues);
static void MenuExitTooHot(prog_values *ProgValues);

#define TOOHOT_H
#endif