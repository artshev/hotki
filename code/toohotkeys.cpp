#include <windows.h>
#include <shellapi.h>
#include <stdint.h>

#define Win32TrayIconMessage (WM_USER + 1)
#define Win32SocketMessage (WM_USER + 2)

typedef int8_t int8;
typedef uint8_t uint8;
typedef int32_t int32;
typedef uint32_t uint32;
typedef int32_t bool32;

#include "toohot.h"

hot_key HotKeys[] =
{ 
    {MOD_ALT | MOD_CONTROL, 'H', OpenHotkeysWindow},
    {MOD_ALT | MOD_CONTROL, 'J', MaximizeLastOpen},
    {MOD_ALT | MOD_CONTROL, 'E', MenuExitTooHot},
};
int32 const HotKeyCount = sizeof(HotKeys)/sizeof(HotKeys[0]);

#if 0
bool32
WriteLog(char *String, char *LogBuffer)
{
    OutputDebugStringA(String); 
    return true;
}
#endif

bool32 
WriteLog(char *String, prog_values *ProgValues)
{
    OutputDebugStringA(String);
    bool32 Result = false;
    
    int32 StringLength = strlen(ProgValues->LogBuffer) + strlen(String) + 1;
    char *TempBuffer = (char *)malloc(StringLength);
    memcpy(TempBuffer, ProgValues->LogBuffer, strlen(ProgValues->LogBuffer)+1);
    strcat(TempBuffer, String);
    free(ProgValues->LogBuffer);
    ProgValues->LogBuffer = strdup(TempBuffer);
    free(TempBuffer);
            
    int32 FileSize = strlen(ProgValues->LogBuffer);
    void *Memory = (void *)ProgValues->LogBuffer;
    
    HANDLE FileHandle = CreateFileA("../toohotLog.txt", GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
    if (FileHandle != INVALID_HANDLE_VALUE)
    {
        DWORD BytesWritten = 0;
        if(WriteFile(FileHandle, Memory, FileSize, &BytesWritten, 0))
        {
            Result = (BytesWritten == FileSize);
            OutputDebugStringA("Writing file succes!!\n");
        }
        else
        {
            OutputDebugStringA("Writing file failed\n");
        }
        
        CloseHandle(FileHandle);
    }
    return(Result);
}

int32  
Win32AddMenuItem(HMENU MenuHandle, char *ItemText,
                 bool32 Checked, bool32 Enabled,
                 void *ExtraData)
{
    // NOTE(Artiem): should the window be checked or not?
    UINT Flags = MF_STRING;
    if(Checked)
    {
        Flags |= MF_CHECKED;
    }
    
    MENUITEMINFO MenuItem;
    MenuItem.cbSize = sizeof(MenuItem);
    MenuItem.fMask = MIIM_ID | MIIM_STATE | MIIM_DATA | MIIM_TYPE;
    MenuItem.fType = MFT_STRING;
    MenuItem.fState = ((Checked ? MFS_CHECKED : MFS_UNCHECKED) |
                       (Enabled ? MFS_ENABLED : MFS_DISABLED));
    MenuItem.wID = GetMenuItemCount(MenuHandle) + 1;
    MenuItem.dwItemData = (LPARAM)ExtraData;
    MenuItem.dwTypeData = (char *)ItemText;
    
    InsertMenuItem(MenuHandle, GetMenuItemCount(MenuHandle),
                   true, &MenuItem);
    
    return(MenuItem.wID);
}

void  
Win32AddSeparator(HMENU MenuHandle)
{
    MENUITEMINFO MenuItem;
    MenuItem.cbSize = sizeof(MenuItem);
    MenuItem.fMask = MIIM_ID | MIIM_DATA | MIIM_TYPE;
    MenuItem.fType = MFT_SEPARATOR;
    MenuItem.wID = 0;
    MenuItem.dwItemData = 0;
    
    InsertMenuItem(MenuHandle, GetMenuItemCount(MenuHandle),
                   true, &MenuItem);
}

void * 
Win32GetMenuItemExtraData(HMENU MenuHandle, int Index)
{
    MENUITEMINFO MenuItemInfo;
    
    MenuItemInfo.cbSize = sizeof(MenuItemInfo);
    MenuItemInfo.fMask = MIIM_DATA;
    MenuItemInfo.dwItemData = 0;
    
    GetMenuItemInfo(MenuHandle, Index, false, &MenuItemInfo);
    
    return((void *)MenuItemInfo.dwItemData);
}

static void 
MaximizeLastOpen(prog_values *ProgValues)
{
    WriteLog(">>MaximizeLastOpen()\r\n", ProgValues);
    HWND Handle = FindWindow(ProgValues->CurrentEditorClassName, ProgValues->LastOpenWindowText);
    if(Handle)
    {
        SetForegroundWindow(Handle);
        ShowWindow(Handle, SW_SHOW);
        
        WriteLog("----maximized: ", ProgValues);
        WriteLog(ProgValues->LastOpenWindowText, ProgValues);
        WriteLog("\r\n", ProgValues);
    }
    else
    {
        WriteLog("----could not find last open window\r\n", ProgValues);
    }
}

VOID CALLBACK 
ExecFileCallback(HWND Window, UINT  Message, ULONG_PTR Data, LRESULT Result)
{
    prog_values *ProgValues = (prog_values *)Data;
    WriteLog(">>ExecFileCallback()\r\n", ProgValues);
    
    SHELLEXECUTEINFO Info = {0};
    Info.cbSize = sizeof(Info);
    Info.fMask = SEE_MASK_NOCLOSEPROCESS;
    Info.lpFile = "notepad";
    Info.nShow = SW_SHOWNORMAL;
    Info.lpParameters = ProgValues->FileToExec;
    
    ShellExecuteEx(&Info);
}

VOID CALLBACK 
CloseTooHotCallback(HWND Window, UINT  Message, ULONG_PTR Data, LRESULT Result)
{
    prog_values *ProgValues = (prog_values *)Data;
    WriteLog(">>CloseTooHotCallback()\r\n", ProgValues);
    PostQuitMessage(0);
}

static void 
CloseLastOpenWindow(SENDASYNCPROC Callback, prog_values *ProgValues)
{
    WriteLog(">>CloseLastOpenWindow()\r\n", ProgValues);
    HWND Handle = FindWindow(ProgValues->CurrentEditorClassName, ProgValues->LastOpenWindowText);
    if(Handle)
    {
        SendMessageCallback(Handle, WM_CLOSE, 0, 0, Callback, (ULONG_PTR)ProgValues);

        SetForegroundWindow(Handle);
        // ShowWindow(Handle, SW_SHOW);
             
        WriteLog("----closed last open window\r\n", ProgValues);
    }
    else
    {
        WriteLog("----could not find last open window\r\n", ProgValues);
    }
}

static void
MenuExitTooHot(prog_values *ProgValues)
{
    WriteLog(">>MenuExitTooHot()\r\n", ProgValues);
    CloseLastOpenWindow(ProgValues->Callback, ProgValues);
    PostQuitMessage(0);
}

bool32 FirstTime;
static void 
OpenHotkeysWindow(prog_values *ProgValues)
{   
    /*
    find the foreground window
    get the class name of that window
    check if this window is known
    if known: open matching content instance
    if unknown: add target window, create new content instance
    */
    WriteLog("\r\n>>OpenHotKeysWindow()\r\n", ProgValues);
    
    HWND TopWindow = GetForegroundWindow();
    if(TopWindow  && IsWindow(TopWindow))
    {
        char TempClassName[256] = {0}; 
        char TempWindowText[256] = {0};
        
        if(GetClassNameA(TopWindow, TempClassName, 256) > 0 )
        {
            ProgValues->TopWindowClassName = strdup(TempClassName);
            
            GetWindowText(TopWindow, TempWindowText, 256);
            // Note(Tjom): checking the value of pointer????
            if(*TempWindowText)
            {
                ProgValues->TopWindowWindowText = strdup(TempWindowText);
            }
            else
            {
                ProgValues->TopWindowWindowText = strdup("not available");
            }
            
            WriteLog("----last open content instance: ", ProgValues);
            WriteLog(ProgValues->LastOpenWindowText, ProgValues);
            WriteLog("\r\n", ProgValues);
            
            WriteLog("----top window target: ", ProgValues);
            WriteLog(ProgValues->TopWindowClassName, ProgValues);
            WriteLog("\r\n", ProgValues);
            
            bool FoundMatch = false;
            list_of_strings *temp = ProgValues->KnownClassNames;
            while(temp != 0)
            {
                if(strncmp(ProgValues->TopWindowClassName, 
                           temp->String, 20) == 0)
                {
                    WriteLog("----TopWindowClassName IS KNOWN\r\n", ProgValues);
                    
                    char FileName[128];
                    strcpy(FileName, temp->String);
                    strcat(FileName, ".txt");
                    
                    char WindowText[128];
                    strcpy(WindowText, FileName);
                    strcat(WindowText, " - Kladblok");
                    
                    ProgValues->FileToExec = strdup(FileName);
                    ProgValues->TargetWindowText = strdup(WindowText);
                    
                    FoundMatch = true;
                    
                    break;
                }
                temp = temp->Next;
            }
            
            if(!FoundMatch)
            {
                WriteLog("TopWindowClassName IS UNKNOWN\r\n", ProgValues);
                char FileName[128];
                strcpy(FileName, ProgValues->TopWindowClassName);
                strcat(FileName, ".txt");
                
                int32 StringLength = strlen(FileName) + strlen(" - Kladblok") + 1;
                char *String = (char *)malloc(StringLength);
                strcpy(String, ProgValues->TopWindowClassName);
                strcat(String, ".txt - Kladblok");
                
                list_of_strings *TempNode = (list_of_strings *)malloc(sizeof list_of_strings);
                TempNode->String = strdup(String);
                TempNode->Next = ProgValues->ContentNames;
                ProgValues->ContentNames = TempNode;
                TempNode = 0;
                WriteLog("----added new content instance\r\n", ProgValues);
                
                list_of_strings *TempNode2 = (list_of_strings *)malloc(sizeof list_of_strings);
                TempNode2->String = strdup(ProgValues->TopWindowClassName);
                TempNode2->Next = ProgValues->KnownClassNames;
                ProgValues->KnownClassNames = TempNode2;
                TempNode2 = 0;
                WriteLog("----added new known window target\r\n", ProgValues);
                
                ProgValues->FileToExec = strdup(FileName);
                ProgValues->TargetWindowText = strdup(String);
            }
            
            HWND ContentInstance = 0;
            temp = ProgValues->ContentNames;
            if(!FirstTime)
            {
                while(temp != 0)
                {
                    if(ContentInstance = FindWindow(ProgValues->CurrentEditorClassName, temp->String))
                    {
                        WriteLog("----found window: ", ProgValues);
                        WriteLog(temp->String, ProgValues);
                        WriteLog("\r\n", ProgValues);
                        break;
                    }
                    temp = temp->Next;
                }
                WriteLog("----succesfully looped through content instances\r\n", ProgValues);
            }
            
            if(ContentInstance)
            {
                if(strncmp(ProgValues->LastOpenWindowText, ProgValues->TargetWindowText, 20) == 0)
                {
                    MaximizeLastOpen(ProgValues);
                }
                else
                {
                    CloseLastOpenWindow(ExecFileCallback, ProgValues);
                    ProgValues->LastOpenWindowText = strdup(ProgValues->TargetWindowText);
                }
            }
            else
            {
                SHELLEXECUTEINFO Info = {0};
                Info.cbSize = sizeof(Info);
                Info.fMask = SEE_MASK_DEFAULT;
                Info.lpFile = "notepad";
                Info.nShow = SW_SHOWNORMAL;
                Info.lpParameters = ProgValues->FileToExec;
                
                ShellExecuteEx(&Info);
                ProgValues->LastOpenWindowText = strdup(ProgValues->TargetWindowText);
                
                FirstTime = false;
                WriteLog("----openend matching content instance\r\n", ProgValues);
            }
        }
        else
        {
            WriteLog("failed to get ClassName\r\n", ProgValues);
        }
    }
    else
    {             
        WriteLog("failed to get window\r\n", ProgValues);
    }
}

typedef void menu_callback(prog_values *ProgValues);
LRESULT CALLBACK
TrayWindowCallback(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
    LRESULT Result = 0;
    prog_values *This;   
    if(Message == WM_CREATE)
    {
        CREATESTRUCT *CreateStruct = (CREATESTRUCT *)LParam;
        This = (prog_values *)CreateStruct->lpCreateParams;
        SetWindowLongPtr(Window, GWLP_USERDATA, (LONG_PTR)This);
    }
    else
    {
        This = (prog_values *)GetWindowLongPtr(Window, GWLP_USERDATA);
    }
    
    switch(Message)
    {   
        case Win32TrayIconMessage:
        {
            switch(LParam)              
            {           
                case WM_RBUTTONDOWN:
                {
                    POINT MousePosition = {0, 0};
                    GetCursorPos(&MousePosition);
                    
                    HMENU WindowMenu = CreatePopupMenu();
                    
                    char Buf[256];
                    wsprintf(Buf, "ClassName: %s", This->TopWindowClassName); 
                    Win32AddMenuItem(WindowMenu, Buf, false, true, 0);
                    wsprintf(Buf, "WindowText: %s", This->TopWindowWindowText); 
                    Win32AddMenuItem(WindowMenu, Buf, false, true, 0);
                    wsprintf(Buf, "LastOpen: %s", This->LastOpenWindowText); 
                    Win32AddMenuItem(WindowMenu, Buf, false, true, 0);
                    
                    Win32AddSeparator(WindowMenu);
                    Win32AddMenuItem(WindowMenu, "Close this menu", false, true, 0);
                    Win32AddMenuItem(WindowMenu, "Exit TooHotKeys", false, true, MenuExitTooHot);
                    
                    int32 PickedIndex = 
                        TrackPopupMenu(WindowMenu,
                                       TPM_LEFTBUTTON |
                                       TPM_NONOTIFY |
                                       TPM_RETURNCMD |
                                       TPM_CENTERALIGN |
                                       TPM_TOPALIGN,
                                       MousePosition.x, 
                                       MousePosition.y,
                                       0, Window, 0);
                    
                    menu_callback *Callback = 
                        (menu_callback *)Win32GetMenuItemExtraData(WindowMenu, PickedIndex);
                    
                    if(Callback)
                    {
                        Callback(This);
                    }
                    
                    DestroyMenu(WindowMenu);
                } break;
                
                default:
                {
                    // An ignored tray message
                } break;
            }
            
        } break;
        
        case WM_HOTKEY:
        {
            int32 HotKeyIndex = WParam;
            if(HotKeyIndex < HotKeyCount)
            {
                if(HotKeys[HotKeyIndex].Callback)
                {
                    HotKeys[HotKeyIndex].Callback(This);
                }
            }
        } break;
        
        default:
        {
            Result = DefWindowProc(Window, Message, WParam, LParam);
        } break;        
    }
    
    return(Result);
}

int CALLBACK
WinMain(HINSTANCE Instance,
        HINSTANCE PrevInstance,
        LPSTR CommandLine,
        int ShowCode)
{
    
    char *TrayWindowClassName = "TooHot";
    
    WNDCLASSEX WindowClass = {sizeof(WindowClass)};
    WindowClass.style = CS_HREDRAW | CS_VREDRAW;
    WindowClass.lpfnWndProc = TrayWindowCallback;
    WindowClass.hInstance = Instance;
    WindowClass.hIcon = LoadIcon(WindowClass.hInstance, MAKEINTRESOURCE(101));
    WindowClass.hCursor = LoadCursor(0, IDC_ARROW);
    WindowClass.lpszClassName = TrayWindowClassName;
    WindowClass.lpszMenuName = "test tray window"; 
    
    if(RegisterClassEx(&WindowClass))
    {
        // TODO(TJOM): use calloc for zero-initialization
        prog_values *ProgValues = (prog_values *)malloc(sizeof prog_values);
        
        ProgValues->TopWindowClassName = strdup("nothing yet");
        ProgValues->TopWindowWindowText = strdup("nothing yet");
        ProgValues->LastOpenWindowText = strdup("nothing yet");
        ProgValues->CurrentEditorClassName = strdup("Notepad");
        ProgValues->FileToExec = 0;
        ProgValues->TargetWindowText = 0;
        ProgValues->Callback = CloseTooHotCallback;
        ProgValues->LogBuffer = strdup("start of log\r\n");
        ProgValues->ContentNames = 0;
        ProgValues->KnownClassNames = 0;
        
        FirstTime = true;
                
        HWND TrayWindow = CreateWindowEx(0, TrayWindowClassName, 0, 0,
                                         0, 0, 1, 1,
                                         0, 0, Instance, (void *)ProgValues);
        
        if(TrayWindow)
        {
            WriteLog("Created TrayWindow\r\n", ProgValues);
            if(SetCurrentDirectory("toohotdata/") == 0)
            {
                CreateDirectory("toohotdata/", 0);
                SetCurrentDirectory("toohotdata/");
                WriteLog("--no data folder, creating new\r\n", ProgValues);
            }
            else
            {
                WriteLog("working dir set secces\r\n", ProgValues);
            }
            
            WIN32_FIND_DATA FileData = {0};
            HANDLE SearchHandle = FindFirstFile("*.txt", &FileData);
            if(SearchHandle != INVALID_HANDLE_VALUE)
            {
                WriteLog("\r\nScanning toohotdada\r\n", ProgValues);
                while(SearchHandle)
                {  
                    WriteLog("----", ProgValues);
                    WriteLog(FileData.cFileName, ProgValues);
                    WriteLog("\r\n", ProgValues);
                    
                    // grab text names and stuff them in list
                    char ContentName[128];
                    strcpy(ContentName, FileData.cFileName);
                    strcat(ContentName, " - Kladblok");                      
                    
                    list_of_strings *NewNode = 
                        (list_of_strings *)malloc(sizeof list_of_strings);
                    NewNode->String = strdup(ContentName);
                    NewNode->Next = ProgValues->ContentNames;
                    ProgValues->ContentNames = NewNode;
                    NewNode = 0;
                    
                    // remove file extention to make class names
                    char TargetName[128];
                    char *Token = strtok(FileData.cFileName, ".");
                    strcpy(TargetName, Token);
                    
                    list_of_strings *TempNode = 
                        (list_of_strings *)malloc(sizeof list_of_strings);
                    TempNode->String = strdup(TargetName);
                    TempNode->Next = ProgValues->KnownClassNames;
                    ProgValues->KnownClassNames = TempNode;
                    TempNode = 0;
                    
                    if(!FindNextFile(SearchHandle, &FileData)) 
                        break;
                } 
                FindClose(SearchHandle);
            }
            else
            {
                WriteLog("data folder is empty\r\n", ProgValues);
            }
            
            for(int32 HotKeyIndex = 0;
                HotKeyIndex < HotKeyCount;
                ++HotKeyIndex)
            {
                hot_key &HotKey = HotKeys[HotKeyIndex];
                RegisterHotKey(TrayWindow, HotKeyIndex, HotKey.Modifiers, HotKey.VKCode);
            }
            
            // Insert ourselves into the system tray
            static NOTIFYICONDATA TrayIconData;
            TrayIconData.cbSize = sizeof(NOTIFYICONDATA); 
            TrayIconData.hWnd = TrayWindow;
            TrayIconData.uID = 0;
            TrayIconData.uFlags = NIF_MESSAGE | NIF_ICON;
            TrayIconData.uCallbackMessage = Win32TrayIconMessage;
            TrayIconData.hIcon = LoadIcon(GetModuleHandle(0), MAKEINTRESOURCE(101));
            TrayIconData.szTip[0] = '\0';
            
            Shell_NotifyIcon(NIM_ADD, &TrayIconData);
            
            MSG Message;
            while(GetMessage(&Message, 0, 0, 0) > 0)
                //while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
            {
                TranslateMessage(&Message);
                DispatchMessage(&Message);
            }
            
            Shell_NotifyIcon(NIM_DELETE, &TrayIconData);       
            
            for(int32 HotKeyIndex = 0;
                HotKeyIndex < HotKeyCount;
                ++HotKeyIndex)
            {
                hot_key &HotKey = HotKeys[HotKeyIndex];
                UnregisterHotKey(TrayWindow, HotKeyIndex);
            }
        }   
    }
   ExitProcess(0);    
};