@echo off

IF NOT DEFINED clset (call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x64)
SET clset=64

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

SET OPTS=/FC /Zi /nologo
SET LINKS=user32.lib gdi32.lib shell32.lib Ole32.lib

REM taskkill /F /IM toohotkeys.exe /T

rc -fo .\toohotkeys.res ..\code\toohotkeys.rc
cl %OPTS% ..\code\toohotkeys.cpp toohotkeys.res %LINKS%

move toohotkeys.exe ../toohotkeys.exe 

popd
 

